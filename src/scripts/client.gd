extends Node


const player_controller = preload("res://entities/fairydust.tscn")

var peer: WebSocketClient


signal init_done


func _ready() -> void:
    assert(get_tree().connect("network_peer_connected", self, "_peer_connected") == OK)
    assert(get_tree().connect("network_peer_disconnected", self, "_peer_disconnected") == OK)
    assert(get_tree().connect("connected_to_server", self, "_connected_ok") == OK)
    assert(get_tree().connect("connection_failed", self, "_connected_fail") == OK)
    assert(get_tree().connect("server_disconnected", self, "_close_network") == OK)

    start_connection()

func start_connection() -> void:
    # Overwrite host with HOST env var if it is set
    var host: String
    if OS.get_environment("HOST") == "":
        host = $"/root/Globals".client_host_default
    else:
        host = OS.get_environment("HOST")

    # Overwrite port with PORT env var if it is set
    var port: int
    if OS.get_environment("PORT") == "":
        port = $"/root/Globals".client_port_default
    else:
        port = OS.get_environment("PORT") as int

    # Connect to server
    peer = WebSocketClient.new()
    print("Connecting to ", host, ":", port)
    assert(peer.connect_to_url(host + ":" + String(port), PoolStringArray(["creaturesofthecyberverse_" + $"/root/Globals".version]), true) == OK)
    get_tree().set_network_peer(peer)

func _connected_ok() -> void:
    print("Connected")

    emit_signal("init_done") # Will trigger the final init of the universe


func _connected_fail() -> void:
    print("Connection failed (Do you have the latest version?), reconnecting...")
    yield(get_tree().create_timer(1), "timeout")

    start_connection() # Dirty retry


func _close_network() -> void:
    print("Connection closed, reconnecting...")
    yield(get_tree().create_timer(1), "timeout")

    start_connection() # Dirty retry


func _peer_connected(id) -> void:
    if id != 1 and id != get_tree().get_network_unique_id(): # The server is not a player
        print("Client ", id, " connected")

        # Spawn new player
        var new_player := player_controller.instance()
        new_player.name = String(id)
        new_player.player_network_id = id
        new_player.set_network_master(1)
        $"/root/universe/players".add_child(new_player)


func _peer_disconnected(id) -> void:
    print("Client ", id, " disconnected")

    # Remove player
    get_node("/root/universe/players/" + String(id)).queue_free()
