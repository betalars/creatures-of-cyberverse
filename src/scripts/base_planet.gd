extends RigidBody


export (float) var G: float = 1
export (bool) var calculate_starting_velocity: bool = false  # Don't use when having multiple gravity_parents
export (Vector3) var starting_velocity_direction: Vector3 = Vector3(0, 1, 0)
export (int) var orbit_polygon_count: int = 200
export (String) var gravity_parent_group: String

var has_orbit_hud: bool = false

puppet var last_transform: Transform;


func _ready() -> void:
    last_transform = self.transform
    $OrbitHud.mesh = null

    # Wait for the network to get ready
    if not $"/root/main".network_ready:
        yield($"/root/main", "network_ready")

    # Make the server the network master
    set_network_master(1)

    # Check if self ist master/server
    if is_network_master():
        # Calculate and apply initiall velocity
        if calculate_starting_velocity and gravity_parent_group:
            var parent = get_tree().get_nodes_in_group(gravity_parent_group)[0]
            self.linear_velocity = starting_velocity_direction.normalized() * sqrt((G * parent.mass) / (self.global_transform.origin - parent.global_transform.origin).length())
    else:
        # Make self kinematic body / immovable
        self.mode = RigidBody.MODE_KINEMATIC

        # Init orbit visulaisation
        if gravity_parent_group:
            var parents = get_tree().get_nodes_in_group(gravity_parent_group)
            if parents.size() == 1:
                var circle_vertecies := PoolVector3Array()
                var th := 0.0
                for i in range(orbit_polygon_count):
                    th = i * TAU / float(orbit_polygon_count)
                    var vertex := Vector3(sin(th), cos(th), 0.0)
                    circle_vertecies.append(vertex)

                var points_mesh := ArrayMesh.new()
                var arrays := []
                arrays.resize(ArrayMesh.ARRAY_MAX)
                arrays[ArrayMesh.ARRAY_VERTEX] = circle_vertecies
                points_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINE_LOOP, arrays, [], VisualServer.ARRAY_FORMAT_VERTEX)
                $OrbitHud.mesh = points_mesh
                var parent = parents[0]
                var parent_to = self.global_transform.origin - parent.global_transform.origin
                $OrbitHud.global_transform = Transform(Basis(starting_velocity_direction.cross(parent_to)).scaled(Vector3(1, 1, 1) * parent_to.length()), parent.global_transform.origin)

                has_orbit_hud = true


func _physics_process(_delta: float) -> void:
    if get_parent() == null: # Wait if not jet in scene tree
        return

    # Apply gravity
    if is_network_master() and gravity_parent_group:
        for parent in get_tree().get_nodes_in_group(gravity_parent_group):
            if parent is RigidBody and parent != self and parent.global_transform.origin != self.global_transform.origin:
                self.add_central_force((parent.global_transform.origin - self.global_transform.origin).normalized()
                                       * (G * parent.mass / pow((self.global_transform.origin - parent.global_transform.origin).length(), 2)))

        rset("last_transform", self.transform)

    if not is_network_master():
        self.transform = self.last_transform

        if has_orbit_hud:
            $OrbitHud.global_transform.origin = get_tree().get_nodes_in_group(gravity_parent_group)[0].global_transform.origin

            $OrbitHud.visible = $"/root/HudSingleton".active
