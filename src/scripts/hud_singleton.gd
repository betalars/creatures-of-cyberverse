extends Node

var active: bool = true setget _set_active
var focused_planet_index: int = -1
var focused_planet: Node = null setget _set_focused_planet

signal hud_active_change(active)
signal hud_focused_planet_change(focused_planet)

func _ready() -> void:
    self.active = self.active # Trigger signal

func _input(event: InputEvent) -> void:
    if event.is_action_pressed("hud_toggle"):
        self.active = !self.active
    elif event.is_action_pressed("hud_focus_next") or event.is_action_pressed("hud_focus_prev"):
        if event.is_action_pressed("hud_focus_next"):
            self.focused_planet_index += 1
        elif event.is_action_pressed("hud_focus_prev"):
            self.focused_planet_index -= 1

        if self.focused_planet_index < -1:
            self.focused_planet_index = get_tree().get_nodes_in_group("focusable").size() - 1
        elif self.focused_planet_index > get_tree().get_nodes_in_group("focusable").size() - 1:
            self.focused_planet_index = -1

        if self.focused_planet_index == -1:
            self.focused_planet = null
        else:
            self.focused_planet = get_tree().get_nodes_in_group("focusable")[self.focused_planet_index]

func _set_active(active_: bool) -> void:
    active = active_
    emit_signal("hud_active_change", self.active)

func _set_focused_planet(focused_planet_: Node) -> void:
    focused_planet = focused_planet_
    emit_signal("hud_focused_planet_change", self.focused_planet)
