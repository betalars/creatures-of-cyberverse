extends Spatial

func _ready() -> void:
    # If the network isn't ready wait for it
    if $"/root/main".network_ready:
        self._main_network_ready()
    else:
        assert($"/root/main".connect("network_ready", self, "_main_network_ready") == OK)


func _main_network_ready() -> void:
    if get_tree().is_network_server():
        # The server doesn't need it's own fairydust
        $"players/fairydust".queue_free()
    else:
        # Rename the player to it's id to make the multiplayer stuff work
        var player: RigidBody = $"players/fairydust"
        player.name = String(get_tree().get_network_unique_id())
        player.player_network_id = get_tree().get_network_unique_id()

        # Start the rocket_api and give it a reference to the player's rocket
        $"/root".call_deferred("add_child", load("res://scripts/rocket_api.gd").new(player))

    get_tree().paused = false # Let the physics loose
