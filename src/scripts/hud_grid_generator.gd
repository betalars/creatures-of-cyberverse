tool
extends MultiMeshInstance

export (float) var grid_size = 2.5 setget grid_size_set # Unit length of the sides
export (int) var grid_side_length = 21 setget grid_side_length_set # How many cubes per side are placed

func _ready() -> void:
    generate()
    
    if not $"/root".has_node("HudSingleton"):
        self.queue_free()
        return

    assert($"/root/HudSingleton".connect("hud_focused_planet_change", self, "_hud_focused_planet_change") == OK)

func _process(_delta: float) -> void:
    update()

func generate() -> void:
    # Reset multimesh
    self.multimesh.instance_count = 0

    # Init particle effect
    var offset_step_size = grid_size / grid_side_length

    # Place cubes in the grid
    self.multimesh.instance_count = int(pow(grid_side_length, 3))
    for i in range(self.multimesh.instance_count):
        # I don't know how exactly those formulars work, but I came up with them one evening and they work great
        var origin = Vector3();
        origin.x = i % grid_side_length
        origin.y = int(floor((i - origin.x) / grid_side_length)) % grid_side_length
        origin.z = int(floor(int(floor((i - origin.y) / grid_side_length))) / grid_side_length)
        self.multimesh.set_instance_transform(i,
            Transform(Basis(), (origin / grid_side_length * Vector3(grid_size, grid_size, grid_size)) # Apply size
                - (Vector3(1, 1, 1) * (grid_size - offset_step_size) * 0.5) # Apply offset, so the grid is centered around (0, 0, 0)
            )
        )

    self.multimesh.mesh.surface_get_material(0).set_shader_param('offset_step_size', offset_step_size)
    self.multimesh.mesh.surface_get_material(0).set_shader_param('grid_size', grid_size)

func update() -> void:
    if not Engine.is_editor_hint(): # The editor camera isn't scriptable, so we can't get it's position
        self.global_transform = Transform(Basis(), get_viewport().get_camera().global_transform.origin)
        self.multimesh.mesh.surface_get_material(0).set_shader_param('origin', self.global_transform.origin)
        if $"/root/HudSingleton".focused_planet != null:
            self.multimesh.mesh.surface_get_material(0).set_shader_param('offset', $"/root/HudSingleton".focused_planet.global_transform.origin)

        self.visible = $"/root/HudSingleton".active

func _hud_focused_planet_change(focused_planet: Node) -> void:
    var color: Color = Color(1.0, 1.0, 1.0)
    if focused_planet == null:
        self.multimesh.mesh.surface_get_material(0).set_shader_param('offset', Vector3())
    else:
        color = Color.from_hsv(float($"/root/HudSingleton".focused_planet_index) / get_tree().get_nodes_in_group("focusable").size(), 0.5, 0.5)
    self.multimesh.mesh.surface_get_material(0).set_shader_param('color', color)

func grid_size_set(grid_size_: float) -> void:
    grid_size = grid_size_

    self.generate()

func grid_side_length_set(grid_side_length_: int) -> void:
    grid_side_length = grid_side_length_

    self.generate()
