extends Node


export(int) var port: int = 9080
var _server := TCP_Server.new()
var active_connection: StreamPeerTCP
var buffer: PoolByteArray = []

var rocket: RigidBody;


func _init(rocket_new: RigidBody) -> void:
    # Get player rocket
    self.rocket = rocket_new


func _ready() -> void:
    # Start periodic timer for handeling the tcp server
    var network_timer = Timer.new()
    network_timer.autostart = true
    network_timer.wait_time = 1.0 / 60.0
    assert(network_timer.connect("timeout", self, "_network_timer_timeout") == OK)
    self.add_child(network_timer)

    assert(rocket.connect("status_update", self, "_rocket_status_update") == OK)

    # Start tcp server
    assert(_server.listen(port, "127.0.0.1") == OK)


func _rocket_status_update(transform: Transform, velocity: Vector3, angular_velocity: Vector3, angular_damp: float, unreal: bool, celestial_bodies: Dictionary, players: Dictionary, latest_errors: Array) -> void:
    # If there is a connection, give it a status update
    if active_connection != null:
        var status_update := {
            "self": {
                "transform": transform,
                "velocity": velocity,
                "angular_velocity": angular_velocity,
                "angular_damp": angular_damp,
                "unreal": unreal,
            },
            "celestial_bodies": celestial_bodies,
            "players": players,
            "latest_errors": latest_errors
        }

        # Convert the status_update to JSON and then to utf8
        if active_connection.put_data((JSON.print(preserialize_status_update(status_update)) + "\n").to_utf8()) != OK:
            # If it fails to send terminate the connection
            active_connection.disconnect_from_host()
            active_connection = null


func preserialize_status_update(status_update: Dictionary) -> Dictionary:
    # Slightly recursive function to preserialize the status_update
    for key in status_update.keys():
        var value = status_update[key]
        if value is Dictionary:
            # Recursion!
            status_update[key] = preserialize_status_update(value)
        elif value is Vector3:
            # Write out the vector
            status_update[key] = preserialize_vector(value)
        elif value is Transform:
            # Get the rotation quaternions
            var quat = value.basis.get_rotation_quat()

            # Serialize the transform
            status_update[key] = {
                "origin": preserialize_vector(value.origin),
                "basis": {
                    "x": preserialize_vector(value.basis.x),
                    "y": preserialize_vector(value.basis.y),
                    "z": preserialize_vector(value.basis.z)
                },
                "rotation_quaternions": {
                    "x": quat.x,
                    "y": quat.y,
                    "z": quat.z,
                    "w": quat.w
                }
            }

    return status_update


func preserialize_vector(vector: Vector3) -> Dictionary:
        return {"x": vector.x, "y": vector.y, "z": vector.z}


func _network_timer_timeout():
    # Check for new connection
    var new_connection = _server.take_connection()
    if new_connection is StreamPeerTCP:
        if active_connection == null:
            # If there isn't a connection, jet take it
            active_connection = new_connection

            print('rocket_api: New connection')
        else:
            # If there is a connection already, reject it
            new_connection.disconnect_from_host()

            print('rocket_api: Connection rejected')

    if active_connection != null:
        if active_connection.get_status() == StreamPeerTCP.STATUS_CONNECTED:
            # Check for incomming data and add it to the buffer
            if active_connection.get_available_bytes() > 1:
                buffer.append_array(active_connection.get_data(active_connection.get_available_bytes())[1])
        else:
            # If the connection is not alive anymore terminate it
            active_connection.disconnect_from_host()
            active_connection = null

            print('rocket_api: Connection ended')

    # Convert buffer to utf8 and search for "\n"
    var buffer_utf8 = buffer.get_string_from_utf8()
    buffer_utf8.substr(0, buffer_utf8.rfind("\n"))
    if buffer_utf8.length() > 0:
        # If there was found something, split it and substract it from the buffer
        var new_commands: PoolStringArray = buffer_utf8.split("\n")
        buffer = buffer.subarray(buffer_utf8.length(), -1)

        # send each command to the rocket / server
        var commands: PoolStringArray = []
        for command in new_commands:
            if command.length() > 0:
                commands.append(command.strip_edges())
        if commands.size() > 0:
            rocket.send_commands(commands)
