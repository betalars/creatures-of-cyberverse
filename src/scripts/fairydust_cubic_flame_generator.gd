tool
extends MultiMeshInstance

export (float) var grid_size = 2.5 setget grid_size_set # Unit length of the sides
export (int) var grid_side_length = 21 setget grid_side_length_set # How many cubes per side are placed
export (float) var debug_thruster_power = 1.0 setget debug_thruster_power_set

func _ready() -> void:
    generate()

func generate() -> void:
    # Reset multimesh
    self.multimesh.instance_count = 0

    # Init particle effect
    var offset_step_size = grid_size / grid_side_length

    # Place cubes in the grid
    self.multimesh.instance_count = int(pow(grid_side_length, 3))
    for i in range(self.multimesh.instance_count):
        # I don't know how exactly those formulars work, but I came up with them one evening and they work great
        var origin = Vector3();
        origin.x = i % grid_side_length
        origin.y = int(floor((i - origin.x) / grid_side_length)) % grid_side_length
        origin.z = int(floor(int(floor((i - origin.y) / grid_side_length))) / grid_side_length)
        self.multimesh.set_instance_transform(i,
            Transform(Basis(), (origin / grid_side_length * Vector3(grid_size, grid_size, grid_size)) # Apply size
                - (Vector3(1, 1, 1) * (grid_size - offset_step_size) * 0.5) # Apply offset, so the grid is centered around (0, 0, 0)
            )
        )

    self.multimesh.mesh.surface_get_material(0).set_shader_param('offset_step_size', offset_step_size)

    if Engine.is_editor_hint():
        debug_thruster_power_set(debug_thruster_power)

func update(thruster_global_position: Vector3, flame: Vector3) -> void:
    self.global_transform = Transform(Basis(), thruster_global_position) # So that cull doesn't hide the meshes

    # NOTE This could be compressed into a matrix if needed
    self.multimesh.mesh.surface_get_material(0).set_shader_param('thruster_position', thruster_global_position)
    self.multimesh.mesh.surface_get_material(0).set_shader_param('thruster_power', flame)

func grid_size_set(grid_size_: float) -> void:
    grid_size = grid_size_

    self.generate()

func grid_side_length_set(grid_side_length_: int) -> void:
    grid_side_length = grid_side_length_

    self.generate()

func debug_thruster_power_set(debug_thruster_power_: float) -> void:
    debug_thruster_power = debug_thruster_power_

    if Engine.is_editor_hint():
        self.update($"../thruster_center".global_transform.origin, $"../thruster_center".global_transform.origin.normalized() * debug_thruster_power)
