extends Node


const player_controller = preload("res://entities/fairydust.tscn")

var peer: WebSocketServer


signal init_done


func _ready() -> void:
    print("Server is starting")

    assert(get_tree().connect("network_peer_connected", self, "_peer_connected") == OK)
    assert(get_tree().connect("network_peer_disconnected", self, "_peer_disconnected") == OK)

    # Overwrite host with HOST env var if it is set
    var host: String
    if OS.get_environment("HOST") == "":
        host = $"/root/Globals".server_host_default
    else:
        host = OS.get_environment("HOST")

    # Overwrite port with PORT env var if it is set
    var port: int
    if OS.get_environment("PORT") == "":
        port = $"/root/Globals".server_port_default
    else:
        port = OS.get_environment("PORT") as int

    # Start the server
    peer = WebSocketServer.new()
    peer.bind_ip = host
    assert(peer.listen(port, PoolStringArray(["creaturesofthecyberverse_" + $"/root/Globals".version]), true) == OK)
    print("Listening on ws://", host, ":", port)

    assert(get_tree().connect("server_disconnected", self, "_close_network") == OK)
    get_tree().set_network_peer(peer)

    emit_signal("init_done") # Will trigger the final init of the universe


func _close_network() -> void:
    # Restart on error
    assert(get_tree().change_scene("res://environments/main.tscn") == OK)


func _peer_connected(id) -> void:
    if id != 1: # The server is not a player
        print("Client ", id, " connected")

        # Spawn a new player
        var new_player := player_controller.instance()
        new_player.name = String(id)
        new_player.player_network_id = id
        new_player.set_network_master(1)
        new_player.transform.origin = $"/root/universe/planets/earth".transform.origin + Vector3(0, 1.5, 0) # Move the player on the top of the earth
        new_player.linear_velocity = $"/root/universe/planets/earth".linear_velocity
        $"/root/universe/players".add_child(new_player)


func _peer_disconnected(id) -> void:
    print("Client ", id, " disconnected")

    # Remove the player
    get_node("/root/universe/players/" + String(id)).queue_free()
