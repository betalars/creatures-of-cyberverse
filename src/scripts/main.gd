extends Node


var network_ready = false;

signal network_ready


func _ready() -> void:
    get_tree().paused = true # Pause game to stop physic updates

    # Load server or client based on cmdline_args and wait for it to get ready
    if "--server" in OS.get_cmdline_args():
        var server: Node = load("res://scripts/server.gd").new()
        $"/root".call_deferred("add_child", server)
        yield(server, "init_done")
    else:
        var hud_singleton: Node = load("res://scripts/hud_singleton.gd").new()
        hud_singleton.name = "HudSingleton"
        $"/root".call_deferred("add_child", hud_singleton)
        var matrix_handler: Node = load("res://scripts/client.gd").new()
        $"/root".call_deferred("add_child", matrix_handler)
        yield(matrix_handler, "init_done")

    # Load universe
    $"/root".call_deferred("add_child", load("res://environments/universe.tscn").instance())

    # Notify that the network is ready (/ multiplayer stuff works)
    network_ready = true
    emit_signal("network_ready")
