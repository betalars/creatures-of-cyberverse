extends Spatial


export (float) var speed: float = 0.1
export (float) var zoom_speed: float = 1
export (float) var zoom_min: float = 1
export (float) var zoom_max: float = 100

var camera_rotation_h: float = 0
var camera_rotation_v: float = 0
var camera_zoom: float = 0


func _ready() -> void:
    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED) # Capture mouse
    #$h/v/Camera.add_exception(get_parent())


func _input(event: InputEvent) -> void:
    if event is InputEventMouseMotion:
        camera_rotation_h -= event.relative.x * speed
        camera_rotation_v -= event.relative.y * speed
    if event is InputEventMouseButton:
        if event.button_index == 5: # scroll out
            camera_zoom += zoom_speed
        elif event.button_index == 4: # scroll in
            camera_zoom -= zoom_speed


func _physics_process(_delta: float) -> void:
    # Rotating the cameras parents also rotates the camera, but this gives a but more control
    $h.rotation_degrees.y = camera_rotation_h
    $h/v.rotation_degrees.x = camera_rotation_v

    # multiply or divide z value by zoom
    if camera_zoom > 0:
        $h/v/Camera.translation.z *= camera_zoom
    elif camera_zoom < 0:
        $h/v/Camera.translation.z /= abs(camera_zoom)
    # Limit z value / zoom
    $h/v/Camera.translation.z = min(zoom_max, max(zoom_min, $h/v/Camera.translation.z))
    camera_zoom = 0 # Reset zoom (else the mouse weel would need to put in the same position to keep the zoom level)
