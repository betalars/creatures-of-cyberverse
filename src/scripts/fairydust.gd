extends RigidBody


export (float) var G: float = 1
export (String) var gravity_parent_group: String = "celestial_bodies"
export (float) var max_speed: float = 0.01
export (float) var max_turn_speed: float = 0.0001
export (float) var max_angular_damp: float = 5
export (int) var player_network_id: int = 1
export (bool) var invert_pitch = false
export (bool) var invert_roll = false
export (bool) var invert_yaw = false
export (float) var flame_lerp_t = 5
export (float) var swivel_max_deviation = 1.0

var last_command_cashe
var command_queue: PoolStringArray = []
var command_queue_lock: Mutex = Mutex.new()
var thruster_power: Vector3 = Vector3()
var thruster_disable_time: float = -1
var torque_impulse := Vector3()
var torque_disable_time: float = -1
var dampening_disable_time: float = -1
var animation: Dictionary = {"flame": Vector3()}
puppet var animation_target: Dictionary = {"flame": Vector3()}
puppet var last_transform: Transform
puppet var unreal: bool = false # If set to true the client is free to do what he wants and can just send a position to the server to move, also collision is obviously disabled

# Gets emitted every physics update and has the transformation, velocity, angular_velocity of the player, the members of the group celestial_bodies and other players, also the angular_damp of the player
signal status_update(transform, velocity, angular_velocity, angular_damp, unreal, celestial_bodies, players, latest_errors)


func _ready() -> void:
    last_transform = self.transform

    # Wait for the network to get ready
    if not $"/root/main".network_ready:
        yield($"/root/main", "network_ready")

    set_network_master(1)

    if not is_network_master():
        # If not server, make immovable (because the server will handle the physics and move it)
        self.mode = RigidBody.MODE_KINEMATIC

# processes inouts each frame
func _process(delta) -> void:
    # limits inputs to only player controlled rockets
    if player_network_id == get_tree().get_network_unique_id():
        var commands: PoolStringArray = []

        # Sets thrust force to be max_speed multiplied by the "thrust" action strength
        commands.append("set_thruster {speed} {swivel_x} {swivel_z}".format({"speed": self.max_speed * Input.get_action_strength("thrust"),
                                                                             "swivel_x": self.swivel_max_deviation / sqrt(2) * (Input.get_action_strength("pitch_plus") - Input.get_action_strength("pitch_minus")),
                                                                             "swivel_z": self.swivel_max_deviation / sqrt(2) * (Input.get_action_strength("yaw_plus") - Input.get_action_strength("yaw_minus"))}))

        # creates vector 3 for use in roll control, adjusts for maximum turn speed and inverts any specified axis
        var roll_modifier: Vector3 = Vector3(1, 1, 1)
        if invert_pitch: roll_modifier.x *= -1
        if invert_roll: roll_modifier.y *= -1
        if invert_yaw: roll_modifier.z *= -1
        roll_modifier *= max_turn_speed

        # assembles torque command using positive and negative offset of directional controls
        commands.append("apply_torque {pitch} {roll} {yaw}".format({"pitch": (Input.get_action_strength("pitch_plus") - Input.get_action_strength("pitch_minus")) * roll_modifier.x,
                                                                    "roll": (Input.get_action_strength("roll_plus") - Input.get_action_strength("roll_minus")) * roll_modifier.y,
                                                                    "yaw": (Input.get_action_strength("yaw_plus") - Input.get_action_strength("yaw_minus")) * roll_modifier.z}))

        commands.append("set_stabilizers {stabilizer}".format({"stabilizer": Input.get_action_strength("damp_angular")*max_angular_damp}))

        # used to ony send updates to server, if input has changed.
        if not last_command_cashe == commands:
            last_command_cashe = commands
            send_commands(commands)

    if not is_network_master():
        # Handle particle
        animation["flame"] = lerp(animation["flame"], animation_target["flame"], min(max(flame_lerp_t * delta, 0), 1))

        $ParticleGrid.update($thruster_center.global_transform.origin, self.transform.basis.xform(-animation["flame"]))

func _physics_process(_delta: float) -> void:
    if get_parent() == null: # If not jet in scene tree
        return

    if is_network_master():
        # Handle thruster_disable_time
        if thruster_disable_time > 0 and thruster_disable_time < (OS.get_system_time_msecs() / 1000.0):
            thruster_power = Vector3()
            thruster_disable_time = -1
        if torque_disable_time > 0 and torque_disable_time < (OS.get_system_time_msecs() / 1000.0):
            torque_impulse = Vector3()
            torque_disable_time = -1
        if dampening_disable_time > 0 and dampening_disable_time < (OS.get_system_time_msecs() / 1000.0):
            self.angular_damp = 0
            dampening_disable_time = -1

        # Handle commands
        command_queue_lock.lock() # The command_queue can be accessed from another thread, so the mutex is making it threadsafe
        var errors = []
        for command in command_queue:
            var error = ""
            #print("Command received ", command)
            var arguments = command.split(" ")
            match arguments[0]:
                "set_thruster":
                    if arguments.size() < 4:
                        error = "Too few parameters"
                    else:
                        if arguments.size() > 5:
                            error = "Too many parameters"
                        else:
                            if not arguments[1].is_valid_float():
                                error = "speed is not a valid float"
                            else:
                                if float(arguments[1]) > max_speed:
                                    error = "speed is larger than the max speed"
                                else:
                                    if float(arguments[1]) < 0:
                                        error = "speed is smaller than 0"
                                    else:
                                        if not arguments[2].is_valid_float():
                                            error = "direction_x is not a valid float"
                                        else:
                                            if not arguments[3].is_valid_float():
                                                error = "direction_z is not a valid float"
                                            else:
                                                if Vector2().distance_to(Vector2(float(arguments[2]), float(arguments[3]))) > swivel_max_deviation + 0.001: # Floating point error compensation
                                                    error = "deviation from thrust vector to large"
                                                else:
                                                    var thruster_vector := Vector3(0, 1, 0).rotated(Vector3(1, 0, 0), float(arguments[2])).rotated(Vector3(0, 0, 1), float(arguments[3])).normalized()
                                                    if arguments.size() > 5:
                                                        if not arguments[5].is_valid_float():
                                                            error = "time is not a valid float"
                                                        else:
                                                            if float(arguments[5]) > 0: # Using 0 or -1 for time is interpreted as infinity
                                                                thruster_disable_time = OS.get_system_time_msecs() / 1000.0 + float(arguments[5])
                                                            else:
                                                                thruster_disable_time = -1
                                                            thruster_power = thruster_vector * float(arguments[1])
                                                    else:
                                                        thruster_disable_time = -1
                                                        thruster_power = thruster_vector * float(arguments[1])
                "apply_torque":
                    if arguments.size() < 4:
                        error = "Too few parameters"
                    else:
                        if arguments.size() > 5:
                            error = "Too many parameters"
                        else:
                            if not arguments[1].is_valid_float():
                                error = "force_x is not a valid float"
                            else:
                                if not arguments[2].is_valid_float():
                                    error = "force_y is not a valid float"
                                else:
                                    if not arguments[3].is_valid_float():
                                        error = "force_z is not a valid float"
                                    else:
                                        if abs(float(arguments[1])) > max_turn_speed:
                                            error = "force_x is larger than the max rotation force"
                                        else:
                                            if abs(float(arguments[2])) > max_turn_speed:
                                                error = "force_y is larger than the max rotation force"
                                            else:
                                                if abs(float(arguments[3])) > max_turn_speed:
                                                    error = "force_z is larger than the max rotation force"
                                                else:
                                                    if arguments.size() > 4:
                                                        if not arguments[4].is_valid_float():
                                                            error = "time is not a valid float"
                                                        else:
                                                            if float(arguments[4]) > 0: # Using 0 or -1 for time is interpreted as infinity
                                                                torque_disable_time = OS.get_system_time_msecs() / 1000.0 + float(arguments[2])
                                                            else:
                                                                torque_disable_time = -1
                                                            torque_impulse = Vector3(float(arguments[1]), float(arguments[2]), float(arguments[3]))
                                                    else:
                                                        torque_disable_time = -1
                                                        torque_impulse = Vector3(float(arguments[1]), float(arguments[2]), float(arguments[3]))
                "set_stabilizers":
                    if arguments.size() < 2:
                        error = "Too few parameters"
                    else:
                        if arguments.size() > 3:
                            error = "Too many parameters"
                        else:
                            if not arguments[1].is_valid_float():
                                error = "damp_intensity is not a valid float"
                            else:
                                if float(arguments[1]) > max_angular_damp:
                                    error = "damp_intensity is larger than the max damp_intensity"
                                else:
                                    if float(arguments[1]) < 0:
                                        error = "damp_intensity is smaller than 0"
                                    else:
                                        if arguments.size() > 2:
                                            if not arguments[2].is_valid_float():
                                                error = "time is not a valid float"
                                            else:
                                                if float(arguments[2]) > 0: # Using 0 or -1 for time is interpreted as infinity
                                                    dampening_disable_time = OS.get_system_time_msecs() / 1000.0 + float(arguments[2])
                                                else:
                                                    dampening_disable_time = -1
                                                    self.angular_damp = float(arguments[1])
                                        else:
                                            dampening_disable_time = -1
                                            self.angular_damp = float(arguments[1])
                "set_unreal":
                    if arguments.size() < 2:
                        error = "Too few parameters"
                    else:
                        if arguments.size() > 2:
                            error = "Too many parameters"
                        else:
                            if arguments[1] != "true" and arguments[1] != "false":
                                error = "Not a valid bool"
                            else:
                                if arguments[1] == "true":
                                    unreal = true
                                    rset("unreal", true)
                                    self.mode = RigidBody.MODE_KINEMATIC
                                    self.collision_layer = 0
                                    self.collision_mask = 0
                                    self.linear_velocity = Vector3()
                                else:
                                    unreal = false
                                    rset("unreal", false)
                                    self.mode = RigidBody.MODE_RIGID
                                    self.collision_layer = 2
                                    self.collision_mask = 2
                "set_transform":
                    if not unreal:
                        error = 'Not allowed in the \"real\" mode'
                    else:
                        if arguments.size() < 13:
                            error = "Too few parameters"
                        else:
                            if arguments.size() > 13:
                                error = "Too many parameters"
                            else:
                                if not arguments[1].is_valid_float():
                                    error = "origin_x is not a valid float"
                                else:
                                    if not arguments[2].is_valid_float():
                                        error = "origin_y is not a valid float"
                                    else:
                                        if not arguments[3].is_valid_float():
                                            error = "origin_z is not a valid float"
                                        else:
                                            if not arguments[4].is_valid_float():
                                                error = "basis_x_x is not a valid float"
                                            else:
                                                if not arguments[5].is_valid_float():
                                                    error = "basis_x_y is not a valid float"
                                                else:
                                                    if not arguments[6].is_valid_float():
                                                        error = "basis_x_z is not a valid float"
                                                    else:
                                                        if not arguments[7].is_valid_float():
                                                            error = "basis_y_x is not a valid float"
                                                        else:
                                                            if not arguments[8].is_valid_float():
                                                                error = "basis_y_y is not a valid float"
                                                            else:
                                                                if not arguments[9].is_valid_float():
                                                                    error = "basis_y_z is not a valid float"
                                                                else:
                                                                    if not arguments[10].is_valid_float():
                                                                        error = "basis_z_x is not a valid float"
                                                                    else:
                                                                        if not arguments[11].is_valid_float():
                                                                            error = "basis_z_y is not a valid float"
                                                                        else:
                                                                            if not arguments[12].is_valid_float():
                                                                                error = "basis_z_z is not a valid float"
                                                                            else:
                                                                                self.transform = Transform(Basis(
                                                                                        Vector3(float(arguments[4]), float(arguments[5]), float(arguments[6])),
                                                                                        Vector3(float(arguments[7]), float(arguments[8]), float(arguments[9])),
                                                                                        Vector3(float(arguments[10]), float(arguments[11]), float(arguments[12]))
                                                                                    ),
                                                                                    Vector3(float(arguments[1]), float(arguments[2]), float(arguments[3])))
                _:
                    error = "Invalid command"

            if error != "":
                errors.append({
                    "command": command,
                    "error": error
                })
        command_queue = []
        command_queue_lock.unlock()

        #print("thruster_power = ", thruster_power, ", thruster_disable_time = ", thruster_disable_time, ", torque_impulse = ", torque_impulse, ", angular_damp = ", self.angular_damp, ", unreal = ", unreal)

        if not unreal:
            # Move player
            #self.apply_impulse(transform.basis.xform($thruster_center.transform.origin), self.transform.basis.xform(thruster_power))
            self.apply_central_impulse(self.transform.basis.xform(thruster_power))
            self.apply_torque_impulse(self.transform.basis.x * torque_impulse.x)
            self.apply_torque_impulse(self.transform.basis.y * torque_impulse.y)
            self.apply_torque_impulse(self.transform.basis.z * torque_impulse.z)

            # Handle animations
            rset("animation_target", {"flame": thruster_power / max_speed})

            # Caclulate and apply gravity from the gravity_parent_group
            if gravity_parent_group:
                for parent in get_tree().get_nodes_in_group(gravity_parent_group):
                    if parent is RigidBody and parent != self and parent.global_transform.origin != self.global_transform.origin:
                        self.add_central_force((parent.global_transform.origin - self.global_transform.origin).normalized()
                                            * (G * parent.mass / pow((self.global_transform.origin - parent.global_transform.origin).length(), 2)))

        # Calculate status_update and send it to the player
        var celestial_bodies = {}
        for celestial_body in get_tree().get_nodes_in_group("celestial_bodies"): # Get celestial_bodies
            if celestial_body is RigidBody and celestial_body != self:
                celestial_bodies[celestial_body.name] = {"transform": celestial_body.global_transform, "velocity": celestial_body.linear_velocity, "angular_velocity": celestial_body.angular_velocity}
        var players = {}
        for player in get_parent().get_children(): # Get players
            if player is RigidBody and player != self:
                players[player.name] = {"transform": player.global_transform, "velocity": player.linear_velocity, "angular_velocity": player.angular_velocity, "unreal": player.unreal}
        # Send the status_update only to the player
        rpc_id(player_network_id, "receive_status", self.transform, self.linear_velocity, self.angular_velocity, self.angular_damp, self.unreal, celestial_bodies, players, errors)

        # Send new position to all clients
        rset("last_transform", self.transform)
    else:
        # Set position to newest position from the server
        self.transform = self.last_transform


func send_commands(commands: PoolStringArray) -> void:
    # Sends commands to server
    rpc_id(1, "receive_commands", commands)


master func receive_commands(commands: PoolStringArray) -> void:
    if get_tree().get_rpc_sender_id() == player_network_id: # Test if the player is in control of this rocket
        command_queue_lock.lock() # The command_queue can be accessed from another thread, so the mutex is making it threadsafe
        self.command_queue.append_array(commands)
        command_queue_lock.unlock()


puppet func receive_status(transform: Transform, velocity: Vector3, angular_velocity: Vector3, angular_damp: float, unreal_status: bool, celestial_bodies: Dictionary, players: Dictionary, latest_errors: Array) -> void:
    for error in latest_errors:
        print("Command error: ", error)

    emit_signal("status_update", transform, velocity, angular_velocity, angular_damp, unreal_status, celestial_bodies, players, latest_errors)
