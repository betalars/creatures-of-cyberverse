shader_type spatial;
uniform sampler2D heat_gradient;
render_mode unshaded;
render_mode blend_add;

uniform vec2 n0_scale = vec2(2.0, 4.0);
uniform vec2 n1_scale = vec2(4, 10);
uniform float time_scale = 3.0;

vec2 random2(vec2 udim_id, float x_mod) {
    if (udim_id.x > (x_mod-1.0))
        {udim_id.x = udim_id.x - x_mod;}
    if (udim_id.x < 0.0)
        {udim_id.x = x_mod-1.0;}
    
	return fract(sin(vec2(
		dot(udim_id, vec2(185.4, 42.0)),
		dot(udim_id, vec2(13.37, 97.5))
	)) * 123.0);
}

float worley2(vec2 coords, float x_mod) {
	float dist = 1.0;
	vec2 mod_coords = fract(coords);
	vec2 udim_id = floor(coords);

	for(int y = -1; y<=1; y++) {
		for(int x = -1; x<=1; x++) {
			vec2 n = vec2(float(x), float(y));
			vec2 diff = n + random2(udim_id + n, x_mod) - mod_coords;
			dist = min(dist, length(diff));
		}
	}
	return dist-0.6;
}

void fragment(){
    float heat = pow(1.0-UV.y,0.7);
    
    heat *= sin(dot(NORMAL, VIEW)*0.7);
    
    heat += worley2(UV*n0_scale-vec2(0, TIME*time_scale*0.5), n0_scale.x)*0.2;
    heat += worley2(UV*n1_scale-vec2(0, TIME*time_scale), n1_scale.x)*0.3*UV.y;
    
    if (heat<0.0){heat = 0.0;}
    
    ALBEDO = pow(mix(texture(heat_gradient, UV).xyz,
                    texture(heat_gradient, vec2(1.0-heat)).xyz,
                    UV.y), vec3(2.2))*heat;
	//ALBEDO = vec3(worley2(UV*n0_scale + vec2(0, TIME), large_scale.x));
} 