shader_type spatial;
render_mode unshaded;

uniform vec3 origin = vec3(0, 0, 0);
uniform float offset_step_size = 10;
uniform float grid_size = 100;
uniform vec3 offset = vec3(0, 0, 0);

uniform float particle_size = 1.0;
uniform float alpha = 0.1;
uniform vec4 color : hint_color = vec4(1.0, 1.0, 1.0, 1.0);
uniform float fade_scale = 3.0;

varying float origin_distance;

void vertex() {
    vec3 vertex_offset = -(mod(origin, offset_step_size) - mod(offset, offset_step_size));
    vec3 world_pos = (WORLD_MATRIX * vec4(VERTEX * 0.0 + vertex_offset, 1.0)).xyz; // absolute position
    origin_distance = distance(world_pos, origin);

    VERTEX = VERTEX * particle_size * 0.1 * clamp((1.0 - origin_distance / ((grid_size - offset_step_size) / 2.0)) * fade_scale, 0.0, 1.0);

    VERTEX = VERTEX + vertex_offset; // Place particle in the right position
}

void fragment() {
    ALBEDO = color.rgb;
    ALPHA = alpha * clamp((1.0 - origin_distance / ((grid_size - offset_step_size) / 2.0)) * fade_scale, 0.0, 1.0);
}
