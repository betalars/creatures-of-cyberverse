shader_type spatial;
render_mode unshaded;

uniform vec3 thruster_position = vec3(0, 0, 0);
uniform vec3 thruster_power = vec3(0, 0, 0);
uniform float offset_step_size = 0.0;

uniform float thruster_acces_distance_base = 0.2; // bigger value => bigger area from thruster acces
uniform float thruster_distance_base = 1.1; // bigger value => bigger area from thruster position
uniform float thruster_distance_base_color = 0.7; // bigger value => less dark from thruster position
uniform float thruster_distance_wrong_direction_mult = 6.0;
uniform float particle_scale = 23.0;
uniform float particle_size_min = 0.4; // Particles with a smaller size are ignored
uniform float particle_size_max = 2.0; // Particles are capped at this size
uniform float noise_weight = 0.6;
uniform float noise_scale = 3.0;
uniform sampler2D texture_file;
uniform float texture_scale = 4.0;
uniform vec2 texture_offset = vec2(0.5, 0.0);
uniform vec4 color_from : hint_color = vec4(0.1, 1.0, 0.61, 1.0);
uniform vec4 color_to : hint_color = vec4(0.1, 0.02, 0.3, 1.0);
uniform vec3 color_noise_scale = vec3(10.0, 1.0, 10.0);
uniform float color_noise_weight = 0.5;
uniform bool debug_view = false;
uniform bool debug_movement = false;
uniform bool debug_rotation = false;

varying float thruster_distance;
varying vec3 vertex_world_pos;
varying vec3 debug_direction_offset;

float distance_line_point(vec3 line_origin, vec3 line_dir, vec3 point) {
    vec3 origin_distance = line_origin - point;
    float area = length(cross(line_dir, origin_distance));
    return area / length(line_dir);
}

// https://www.shadertoy.com/view/Xsl3Dl
vec3 hash_3d(vec3 p) {
    p = vec3( dot(p,vec3(127.1,311.7, 74.7)),
              dot(p,vec3(269.5,183.3,246.1)),
              dot(p,vec3(113.5,271.9,124.6)));

    return -1.0 + 2.0*fract(sin(p)*43758.5453123);
}

float noise_3d(vec3 p) {
    vec3 i = floor( p );
    vec3 f = fract( p );
    
    vec3 u = f*f*(3.0-2.0*f);

    return mix( mix( mix( dot( hash_3d( i + vec3(0.0,0.0,0.0) ), f - vec3(0.0,0.0,0.0) ), 
                          dot( hash_3d( i + vec3(1.0,0.0,0.0) ), f - vec3(1.0,0.0,0.0) ), u.x),
                     mix( dot( hash_3d( i + vec3(0.0,1.0,0.0) ), f - vec3(0.0,1.0,0.0) ), 
                          dot( hash_3d( i + vec3(1.0,1.0,0.0) ), f - vec3(1.0,1.0,0.0) ), u.x), u.y),
                mix( mix( dot( hash_3d( i + vec3(0.0,0.0,1.0) ), f - vec3(0.0,0.0,1.0) ), 
                          dot( hash_3d( i + vec3(1.0,0.0,1.0) ), f - vec3(1.0,0.0,1.0) ), u.x),
                     mix( dot( hash_3d( i + vec3(0.0,1.0,1.0) ), f - vec3(0.0,1.0,1.0) ), 
                          dot( hash_3d( i + vec3(1.0,1.0,1.0) ), f - vec3(1.0,1.0,1.0) ), u.x), u.y), u.z );
}

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

mat3 mat3_from_acis_angle(float angle, vec3 axis) {
    float s = sin(angle);
    float c = cos(angle);
    float t = 1.0 - c;
    float x = axis.x;
    float y = axis.y;
    float z = axis.z;
    return mat3(
        vec3(t*x*x+c, t*x*y-s*z, t*x*z+s*y),
        vec3(t*x*y+z*s, t*y*y+c, t*y*z-s*x),
        vec3(t*x*z-s*y, t*y*z+s*z, t*z*z+c)
    );
}

void vertex() {
    vec3 debug_offset = debug_movement ? vec3(mod(sin(TIME * 0.3), offset_step_size), mod(- TIME * 0.5, offset_step_size), mod(sin((TIME + 2.0) * 0.3), offset_step_size)) : vec3(0.0);
    debug_direction_offset = debug_rotation ? vec3(sin(TIME * 0.3), 0.0, cos(TIME * 0.3)) * 0.5 : vec3(0.0);
    vec3 world_pos = (WORLD_MATRIX * vec4(VERTEX * 0.0 - mod(thruster_position, offset_step_size) + debug_offset, 1.0)).xyz; // absolute position
    thruster_distance = distance(world_pos, thruster_position); // distance to thruster position
    float thruster_acces_distance = distance_line_point(thruster_position, normalize(thruster_power) + debug_direction_offset, world_pos); // distance to the line going from the thruster in thrusting direction
    bool in_thrust_direction = dot(world_pos - thruster_position, normalize(thruster_power) + debug_direction_offset) > 0.0;

    // Scale the cube
    float particle_size = clamp((
            clamp(thruster_acces_distance_base - thruster_acces_distance, 0.0, 1.0)
            * clamp(thruster_distance_base - thruster_distance * (in_thrust_direction ? 1.0 : thruster_distance_wrong_direction_mult), 0.0, 1.0)
        ) * particle_scale
        * (noise_3d(world_pos * noise_scale) * noise_weight + (1.0 - noise_weight))
        * length(thruster_power)
        , 0.0, particle_size_max);

    if (debug_view) {
        particle_size = clamp(particle_size, 0.05, particle_size_max);
    }

    // Ignore too small particles
    if (particle_size > particle_size_min || debug_view) {
        VERTEX = VERTEX * particle_size * 0.1;
    } else {
        VERTEX = VERTEX * 0.0;
    }

    VERTEX = VERTEX - mod(thruster_position, offset_step_size) + debug_offset; // Place particle in the right position

    vertex_world_pos = (WORLD_MATRIX * vec4(VERTEX - mod(thruster_position, offset_step_size) + debug_offset, 1.0)).xyz;
}

void fragment() {
    ALBEDO = texture(texture_file, UV * texture_scale + texture_offset).xyz
        * hsv2rgb(mix(rgb2hsv(color_to.rgb), rgb2hsv(color_from.rgb),
            clamp(clamp(thruster_distance_base_color - thruster_distance, 0.0, 1.0)
                //+ (noise_3d(vertex_world_pos * (color_noise_scale * mat3_from_acis_angle(0.0, normalize(thruster_power) + debug_direction_offset))) - 0.0) * color_noise_weight // FIXME color_noise_scale is not beeing roated even thou it should
            , 0.0, 1.0)));
}
