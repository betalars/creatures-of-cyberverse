extends RigidBody
export (float) var atmosphere_density = 0.5
export (Vector3) var travel_direction = Vector3(0, 10, 1)


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
    pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    transform.origin = transform.origin.slerp(get_parent().transform.origin, 1-delta)

func _physics_process(delta):
    
    var up = global_transform.basis.y
    
    # var mieps = atan2(target_vector.z, target_vector.x)
    

    
    #apply_torque_impulse((travel_direction.normalized()-up)*delta*delta)
