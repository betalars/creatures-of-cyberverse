# Creatures of Cyberverse

A chaotic social rocket science experience

## Controls
q, w, e, a, s, d: for rotation  
space: for thruster  
shift: for rotation dampening  
tab: toggle hud  
page up, page down: change hud focus
## Running the server
- Install godot-headless
- `$ cd src`
- `$ godot-headless -- --server`
## Rocket api
This api listens as a TCP server on 127.0.0.1 (Godot doesn't understand what localhost is) port 9080.
It will let one peer connect and no more.
It will send the connected peer a status update every ~0.16ms in json. Example:
```json
{
    "self": {
        "transform": {
            "origin": {
                "x": -18.931004,
                "y": 9.47361,
                "z": -0.172453
            },
            "basis": {
                "x": {
                    "x": 0.713424,
                    "y": -0.017909,
                    "z": 0.700504
                },
                "y": {
                    "x": -0.42989,
                    "y": -0.800633,
                    "z": 0.417351
                },
                "z": {
                    "x": 0.553372,
                    "y": -0.598888,
                    "z": -0.578889
                }
            },
            "rotation_quaternions": {
                "x": 0.879339,
                "y": -0.127311,
                "z": 0.356483,
                "w": 0.288921
            }
        },
        "velocity": {
            "x": 0.317495,
            "y": 0.63221,
            "z": -0.012355
        },
        "angular_velocity": {
            "x": -0.025331,
            "y": -0.068132,
            "z": -0.001378
        },
        "angular_damp": 0,
        "unreal": false
    },
    "celestial_bodies": {
        "earth": {
            "transform": {
                "origin": {
                    "x": -17.883404,
                    "y": 8.959531,
                    "z": 0
                },
                "basis": {
                    "x": {
                        "x": -0.956957,
                        "y": 0.289945,
                        "z": -0.012838
                    },
                    "y": {
                        "x": -0.289969,
                        "y": -0.957036,
                        "z": 0
                    },
                    "z": {
                        "x": -0.012287,
                        "y": 0.003723,
                        "z": 0.999918
                    }
                },
                "rotation_quaternions": {
                    "x": -0.00635,
                    "y": 0.000941,
                    "z": 0.98918,
                    "w": 0.146564
                }
            },
            "velocity": {
                "x": 0.316353,
                "y": 0.632302,
                "z": 0
            },
            "angular_velocity": {
                "x": 0,
                "y": 0,
                "z": -0.3
            }
        },
        ...
    },
    "players": {
        "988693882": {
            "transform": {
                "origin": {
                    "x": -17.89959,
                    "y": 10.439096,
                    "z": 0.004
                },
                "basis": {
                    "x": {
                        "x": 0.999899,
                        "y": 0.014246,
                        "z": -1.3e-05
                    },
                    "y": {
                        "x": -0.014245,
                        "y": 0.999867,
                        "z": 0.007901
                    },
                    "z": {
                        "x": 0.000125,
                        "y": -0.0079,
                        "z": 0.999969
                    }
                },
                "rotation_quaternions": {
                    "x": 0.003951,
                    "y": 3.5e-05,
                    "z": 0.007123,
                    "w": 0.999967
                }
            },
            "velocity": {
                "x": 0,
                "y": 0,
                "z": 0
            },
            "angular_velocity": {
                "x": 0,
                "y": 0,
                "z": 0
            },
            "unreal": true
        },
        ...
    },
    "last_errors": [
        {
            "command": "invalid command lol",
            "error": "Invalid command"
        },
        ...
    ]
}
```
To control the rocket the peer can send the following commands (The `<...>` are just placeholders, `[...]` means optional):
- `set_thruster <speed: float> <rotation_x: float> <rotation_z: float> [<time in sec: float>]`
  Sets the thrustert to a fixed speed (set to 0 to disable) and the rotation (rotation is in radiants). Time can be be set to stop the thruster after the specified time, gets treated as not set if <= 0.
- `apply_torque <force_x: float> <force_y: float> <force_z: float> [<time in sec: float>]`
  Applies torque to the rocket / Starts turning it. Time can be be set to stop the torque after the specified time, gets treated as not set if <= 0.
- `set_stabilizers <damp_intensity: float> [<time in sec: float>]`
  Sets the angular stabelizers to a fixed dampening intensity (set to 0 to disable). Time can be be set to stop the stabelizers after the specified time, gets treated as not set if <= 0.
- `set_unreal <enable: bool>`
  Disables physics and gives full control to the client (bool is "true" or "false")
- `set_transform <origin_x: float> <origin_y: float> <origin_z: float> <basis_x_x: float> <basis_x_y: float> <basis_x_z: float> <basis_y_x: float> <basis_y_y: float> <basis_y_z: float> <basis_z_x: float> <basis_z_y: float> <basis_z_z: float>`
  (Restricted) Can be used to teleport when server physics are disabled
